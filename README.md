# Photos from the project
<table>
  <tr>
    <td>PC</td>
    <td>Mobile</td>
  </tr>
  <tr>
    <td colspan="1"><img src="https://i.imgur.com/RhATFDv.png" width="400"/></td>
    <td colspan="2"><img src="https://i.imgur.com/uy6Peyi.png" width="300"/></td>
  </tr>
  <tr>
    <td colspan="2">Not Much a Difference, I know!</td>
  </tr>
</table>

# What is new?
* Added Arabic!
* Added and Edited new pages/blogs!
* Fixed and Updated links!
* Used languagetool.org to fix my bad English!
* Beautified my HTML and CSS.
* Nothing, more...
